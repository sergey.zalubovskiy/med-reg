﻿using Autofac;

namespace Registration.Data.Projections
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClinicRegistrationProjection>().AsSelf();
        }
    }
}