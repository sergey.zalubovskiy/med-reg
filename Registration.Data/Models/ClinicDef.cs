using Newtonsoft.Json;

namespace Registration.Data.Models
{
    public class ClinicDef
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}