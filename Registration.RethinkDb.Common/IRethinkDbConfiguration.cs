﻿namespace Registration.RethinkDb.Common
{
    public interface IRethinkDbConfiguration
    {
        string DatabaseName { get; }

        string Host { get; }

        int Port { get; }

        int TimeoutSeconds { get; }
    }
}