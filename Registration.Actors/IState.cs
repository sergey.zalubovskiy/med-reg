﻿using Registration.Events;

namespace Registration.Actors
{
    public interface IState<in TEvent, out TSelf>
        where TEvent : class, IEvent
        where TSelf : class, IState<TEvent, TSelf>
    {
        string AggregateKey { get; }
        ulong SequenceNumber { get; }
        TSelf ApplyEvent(ulong sequenceNumber, TEvent @event);
    }
}