﻿using System;
using Microsoft.Extensions.Logging;
using Proto;
using Proto.Remote;
using Registration.Actors.Common;

namespace Registration.Actors.Clinics
{
    public class ClinicClusterNode : ClusterNode
    {
        private readonly Func<Clinic> _clinicFactory;
        private readonly Func<ClinicRegistry> _clinicRegistryFactory;
        private readonly Func<ClinicRegistryProjection> _projectionFactory;

        public ClinicClusterNode(ILoggerFactory loggerFactory, Func<Clinic> clinicFactory,
            Func<ClinicRegistry> clinicRegistryFactory, Func<ClinicRegistryProjection> projectionFactory) : base(
            loggerFactory)
        {
            _clinicFactory = clinicFactory;
            _clinicRegistryFactory = clinicRegistryFactory;
            _projectionFactory = projectionFactory;
        }

        #region Overrides of ClusterNode

        protected override void RegisterKinds()
        {
            var clinicActor = Actor.FromProducer(_clinicFactory);
            var clinicRegistryActor = Actor.FromProducer(_clinicRegistryFactory);
            Remote.RegisterKnownKind(Clinic.ActorKind, clinicActor);
            Remote.RegisterKnownKind(ClinicRegistry.ActorKind, clinicRegistryActor);
        }

        public override void StartProjection()
        {
            var projection = _projectionFactory();
            projection.Start();
        }

        #endregion
    }
}