﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using Proto.Cluster;
using Proto.Remote;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Events.Clinics.ClinicRegistration;
using Registration.Journals.Errors;
using Registration.Journals.Events;

namespace Registration.Actors.Clinics
{
    public class Result
    {
        public Exception Ex { get; set; }
    }

    public class ClinicRegistrationDispatcher
    {
        private readonly IErrorJournal _errorJournal;
        private readonly IEventJournal _eventJournal;

        public ClinicRegistrationDispatcher(ErrorJournalFactory errorJournalFactory,
            EventJournalFactory eventJournalFactory)
        {
            _eventJournal = eventJournalFactory(typeof(IClinicRegistrationEvent).Name);
            _errorJournal = errorJournalFactory(typeof(IClinicRegistrationEvent).Name);
        }

        public async Task<Result> TellAsync(string trackId, IClinicRegistrationAction action)
        {
            await _errorJournal.EnsureJournalAsync();
            await _eventJournal.EnsureJournalAsync();
            var command = new ClinicRegistrationCommand(trackId, action);
            var (clinic, result) = await Cluster.GetAsync(command.AggregateKey, ClinicRegistry.ActorKind);
            var attempts = 0;
            while (result != ResponseStatusCode.OK)
            {
                (clinic, result) = await Cluster.GetAsync(command.AggregateKey, ClinicRegistry.ActorKind);
                attempts++;
                if (attempts > 1)
                    throw new Exception("Unable to get clinic registration actor");
            }
            clinic.Tell(command);
            var errorTask = _errorJournal
                .Listen(trackId)
                .SubscribeOn(TaskPoolScheduler.Default)
                .Do(c => { })
                .FirstOrDefaultAsync()
                .ToTask();
            var eventTask = _eventJournal
                .Listen(command.AggregateKey, trackId)
                .SubscribeOn(TaskPoolScheduler.Default)
                .FirstOrDefaultAsync()
                .ToTask();

            await Task.WhenAny(errorTask, eventTask);
            if (errorTask.IsCompleted)
            {
                var error = errorTask.Result;
                return new Result
                {
                    Ex = error.Exception
                };
            }
            return new Result();
        }
    }
}