﻿using System;
using System.Threading.Tasks;
using Registration.Actions;
using Registration.Actions.Clinics;
using Registration.Actors.Common;
using Registration.Actors.Implementation;
using Registration.Events.Clinics;

namespace Registration.Actors.Clinics
{
    public class ClinicState : BaseState<IClinicEvent, ClinicState>
    {
        public ClinicState(string aggregateKey) : base(aggregateKey)
        {
        }

        public string ClinicName { get; private set; }

        #region Overrides of BaseState<IClinicEvent,ClinicState>

        protected override ClinicState ApplyEventInternal(IClinicEvent @event)
        {
            switch (@event)
            {
                case CreateClinic create:
                    ClinicName = create.ClinicName;
                    return this;
                default:
                    throw new Exception($"Unknown event {@event.GetType().Name} ");
            }
        }

        #endregion
    }

    public class ClinicCommandHander : ICommandHandler<ClinicState, IClinicEvent>
    {
        #region Implementation of ICommandHandler<in ClinicState,out IClinicEvent>

        public Task InitAsync()
        {
            throw new NotImplementedException();
        }

        public IClinicEvent Handle(ClinicState state, ICommand command)
        {
            try
            {
                switch (command.Action)
                {
                    case CreateClinic create:
                        return new ClinicCreated
                        {
                            ClinicName = create.ClinicName
                        };
                    default:
                        throw new Exception($"Unknown action {command.Action.GetType().Name}");
                }
            }
            catch (Exception ex)
            {
                throw new ProcessingException("Error during command handling", ex, command);
            }
        }

        #endregion
    }

    public class Clinic : AggregateActor<ClinicState, IClinicEvent>
    {
        public Clinic(ClinicCommandHander commandHandler,
            EventSourcedStateFactory<IClinicEvent, ClinicState> eventSourcedStateFactory,
            ErrorHandlerFactory errorHandlerFactory) : base(commandHandler, eventSourcedStateFactory,
            errorHandlerFactory)
        {
        }

        public static string ActorKind => "Clinic";

        #region Overrides of AggregateActor<ClinicState,IClinicEvent>

        protected override ClinicState GetInitalState(string aggregateState)
        {
            return new ClinicState(aggregateState);
        }

        #endregion
    }
}