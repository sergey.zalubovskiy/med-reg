﻿using System;
using System.Collections.Generic;
using Registration.Actions;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Actors.Common;
using Registration.Actors.Implementation;
using Registration.Events.Clinics.ClinicRegistration;

namespace Registration.Actors.Clinics
{
    public class ClinicRegistryState : BaseState<IClinicRegistrationEvent, ClinicRegistryState>
    {
        public ClinicRegistryState(string aggregateKey) : base(aggregateKey)
        {
            Clinics = new Dictionary<string, string>();
        }

        public IDictionary<string, string> Clinics { get; }

        #region Overrides of BaseState<IClinicEvent,ClinicState>

        protected override ClinicRegistryState ApplyEventInternal(IClinicRegistrationEvent @event)
        {
            switch (@event)
            {
                case ClinicRegistered register:
                    Clinics[register.ClinicName.ToUpperInvariant()] = register.ClinicAddress;
                    return this;
                default:
                    throw new Exception($"Unknown event {@event.GetType().Name} ");
            }
        }

        #endregion
    }

    public class ClinicRegistrationCommandHander : ICommandHandler<ClinicRegistryState, IClinicRegistrationEvent>
    {
        #region Implementation of ClinicRegistrationCommandHander<in ClinicRegistryState,out IClinicRegistrationEvent>

        public IClinicRegistrationEvent Handle(ClinicRegistryState state, ICommand command)
        {
            try
            {
                switch (command.Action)
                {
                    case RegisterClinic register:
                        if (state.Clinics.TryGetValue(register.ClinicName.ToUpperInvariant(), out var _))
                            throw new Exception($"Clinic with name {register.ClinicName} already registered");
                        return new ClinicRegistered
                        {
                            Id = $"clinic_{state.Clinics.Values.Count}",
                            ClinicName = register.ClinicName,
                            ClinicAddress = register.ClinicAddress
                        };
                    default:
                        throw new Exception($"Unknown action {command.Action.GetType().Name}");
                }
            }
            catch (Exception ex)
            {
                throw new ProcessingException("Error during command handling", ex, command);
            }
        }

        #endregion
    }

    public class ClinicRegistry : AggregateActor<ClinicRegistryState, IClinicRegistrationEvent>
    {
        public ClinicRegistry(ClinicRegistrationCommandHander commandHandler,
            EventSourcedStateFactory<IClinicRegistrationEvent, ClinicRegistryState> eventSourcedStateFactory,
            ErrorHandlerFactory errorHandlerFactory) : base(commandHandler, eventSourcedStateFactory,
            errorHandlerFactory)
        {
        }

        public static string ActorKind => "ClinicRegistry";

        #region Overrides of AggregateActor<ClinicRegistryState,IClinicRegistrationEvent>

        protected override ClinicRegistryState GetInitalState(string aggregateKey)
        {
            return new ClinicRegistryState(aggregateKey);
        }

        #endregion
    }
}