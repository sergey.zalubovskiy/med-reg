﻿using System.Threading.Tasks;
using Registration.Actions;
using Registration.Actors.Common;
using Registration.Events;

namespace Registration.Actors.Interfaces
{
    public interface IEventSourcedState<in TEvent, TState>
        where TEvent : class, IEvent
        where TState : class, IState<TEvent, TState>
    {
        Task<TState> InitAsync(TState initialState);

        /// <exception cref="PersistenceException"></exception>
        Task<TState> ApplyAsync(TState state, TEvent @event, ICommand command);
    }
}