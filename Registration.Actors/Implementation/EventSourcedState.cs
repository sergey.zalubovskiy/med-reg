﻿using System;
using System.Threading.Tasks;
using Registration.Actions;
using Registration.Actors.Common;
using Registration.Actors.Interfaces;
using Registration.Events;
using Registration.Journals.Events;
using Registration.Journals.Implementations.Events;

namespace Registration.Actors.Implementation
{
    public delegate IEventSourcedState<TEvent, TState> EventSourcedStateFactory<in TEvent, TState>(string journalName)
        where TEvent : class, IEvent
        where TState : class, IState<TEvent, TState>;

    internal class EventSourcedState<TEvent, TState> : IEventSourcedState<TEvent, TState>
        where TEvent : class, IEvent
        where TState : class, IState<TEvent, TState>
    {
        private readonly Func<IEventJournalEntryBuilder> _entryBuilderFactory;
        private readonly IEventJournal _journal;

        public EventSourcedState(string journalName, EventJournalFactory journalFactory,
            Func<IEventJournalEntryBuilder> entryBuilderFactory)
        {
            _journal = journalFactory(journalName);
            _entryBuilderFactory = entryBuilderFactory;
        }

        private TState ApplyEntry(TState state, IEventJournalEntry entry)
        {
            try
            {
                switch (entry.Event)
                {
                    case TEvent @event:
                        return state.ApplyEvent(entry.SequenceNumber, @event);
                    default:
                        throw new Exception($"Unsupported event type {entry.Event.GetType()}");
                }
            }
            catch (Exception ex)
            {
                throw new PersistenceException("Error while initializing state from store", ex, entry);
            }
        }

        #region Implementation of IEventSourcedState<in TEvent,TState>

        public async Task<TState> InitAsync(TState initialState)
        {
            await _journal.EnsureJournalAsync();
            var previousEntries = await _journal.QueryAsync(initialState.AggregateKey, initialState.SequenceNumber);
            var state = initialState;
            foreach (var entry in previousEntries)
                state = ApplyEntry(state, entry);
            return state;
        }

        public async Task<TState> ApplyAsync(TState state, TEvent @event, ICommand command)
        {
            var nextSequenceNumber = state.SequenceNumber + 1;
            var entry = _entryBuilderFactory()
                .WithEvent(@event)
                .WithAction(command.Action)
                .WithSequenceNumber(nextSequenceNumber)
                .WithAggregateKey(command.AggregateKey)
                .WithTrackId(command.TrackId)
                .ToEntry();
            var newState = ApplyEntry(state, entry);
            await _journal.PublishAsync(entry);
            return newState;
        }

        #endregion
    }
}