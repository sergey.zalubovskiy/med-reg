﻿using System;
using Registration.Actions;

namespace Registration.Actors.Common
{
    public class ProcessingException : Exception
    {
        public ProcessingException(string message, Exception innerException, ICommand command)
            : base(message, innerException)
        {
            Command = command;
        }

        public ICommand Command { get; set; }
    }
}