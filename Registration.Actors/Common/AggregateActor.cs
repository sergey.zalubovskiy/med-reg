﻿using System;
using System.Threading.Tasks;
using Proto;
using Registration.Actions;
using Registration.Actors.Implementation;
using Registration.Actors.Interfaces;
using Registration.Events;

namespace Registration.Actors.Common
{
    public interface ICommandHandler<in TState, out TEvent>
        where TState : class, IState<TEvent, TState>
        where TEvent : class, IEvent
    {
        /// <exception cref="ProcessingException"></exception>
        TEvent Handle(TState state, ICommand command);
    }

    public abstract class AggregateActor<TState, TEvent> : IActor
        where TEvent : class, IEvent
        where TState : class, IState<TEvent, TState>
    {
        private readonly ICommandHandler<TState, TEvent> _commandHandler;
        private readonly ErrorHandler _errorHandler;
        private readonly IEventSourcedState<TEvent, TState> _eventSourcedState;
        private TState _currentState;

        protected AggregateActor(ICommandHandler<TState, TEvent> commandHandler,
            EventSourcedStateFactory<TEvent, TState> eventSourcedStateFactory,
            ErrorHandlerFactory errorHandlerFactory)
        {
            var journalName = typeof(TEvent).Name;
            _commandHandler = commandHandler;
            _eventSourcedState = eventSourcedStateFactory(journalName);
            _errorHandler = errorHandlerFactory(journalName);
        }

        public async Task ReceiveAsync(IContext context)
        {
            switch (context.Message)
            {
                case Started _:
                    var aggregateState = context.Self.Id;
                    var initialState = GetInitalState(aggregateState);
                    _currentState = await _eventSourcedState.InitAsync(initialState);
                    await _errorHandler.InitAsync();
                    break;
                case ICommand command:
                    try
                    {
                        var @event = _commandHandler.Handle(_currentState, command);
                        _currentState = await _eventSourcedState.ApplyAsync(_currentState, @event, command);
                    }
                    catch (Exception ex)
                    {
                        await _errorHandler.HandleAsync(ex);
                        context.Self.Tell(Stop.Instance);
                    }
                    break;
            }
        }

        protected abstract TState GetInitalState(string aggregateKey);
    }
}