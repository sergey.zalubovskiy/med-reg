﻿using System;
using Registration.Journals.Events;

namespace Registration.Actors.Common
{
    public class PersistenceException : Exception
    {
        public PersistenceException(string messages, Exception innerException, IEventJournalEntry journalEntry)
            : base(messages, innerException)
        {
            JournalEntry = journalEntry;
        }

        public IEventJournalEntry JournalEntry { get; }
    }
}