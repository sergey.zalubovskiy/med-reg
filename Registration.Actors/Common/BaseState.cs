﻿using Registration.Events;

namespace Registration.Actors.Common
{
    public abstract class BaseState<TEvent, TSelf> : IState<TEvent, TSelf>
        where TEvent : class, IEvent
        where TSelf : BaseState<TEvent, TSelf>, IState<TEvent, TSelf>
    {
        protected BaseState(string aggregateKey)
        {
            AggregateKey = aggregateKey;
        }

        protected abstract TSelf ApplyEventInternal(TEvent @event);

        #region Implementation of IState<in TEvent,out TSelf>

        public string AggregateKey { get; }
        public ulong SequenceNumber { get; private set; }

        public TSelf ApplyEvent(ulong sequenceNumber, TEvent @event)
        {
            var result = ApplyEventInternal(@event);
            SequenceNumber = sequenceNumber;
            return result;
        }

        #endregion
    }
}