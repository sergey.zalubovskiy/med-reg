﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Registration.Journals.Errors;
using Registration.Journals.Implementations.Errors;

namespace Registration.Actors.Common
{
    public delegate ErrorHandler ErrorHandlerFactory(string journalName);

    public class ErrorHandler
    {
        private readonly Func<IActionErrorJournalEntryBuilder> _actionErrorBuilder;
        private readonly IErrorJournal _errorJournal;
        private readonly Func<IEventErrorJournalEntryBuilder> _eventErrorBuilder;
        private readonly ILoggerFactory _loggerFactory;

        public ErrorHandler(string journalName, ILoggerFactory loggerFactory, ErrorJournalFactory errorJournalFactory,
            Func<IActionErrorJournalEntryBuilder> actionErrorBuilder,
            Func<IEventErrorJournalEntryBuilder> eventErrorBuilder)
        {
            _errorJournal = errorJournalFactory(journalName);
            _loggerFactory = loggerFactory;
            _actionErrorBuilder = actionErrorBuilder;
            _eventErrorBuilder = eventErrorBuilder;
        }

        public async Task InitAsync()
        {
            await _errorJournal.EnsureJournalAsync();
        }

        public async Task HandleAsync(Exception reason)
        {
            _loggerFactory.CreateLogger<ErrorHandler>().LogError(0, reason, "Handle error");
            switch (reason)
            {
                case ProcessingException ex:
                    var actionEntry = FromProcessingException(ex);
                    await _errorJournal.PublishActionAsync(actionEntry);
                    return;
                case PersistenceException ex:
                    var eventEntry = FromPersistenceException(ex);
                    await _errorJournal.PublishEventAsync(eventEntry);
                    return;
            }
        }

        private IActionErrorJournalEntry FromProcessingException(ProcessingException ex)
        {
            return _actionErrorBuilder()
                .WithAction(ex.Command.Action)
                .WithAggregateKey(ex.Command.AggregateKey)
                .WithTrackId(ex.Command.TrackId)
                .WithException(ex.InnerException)
                .ToEntry();
        }

        private IEventErrorJournalEntry FromPersistenceException(PersistenceException ex)
        {
            return _eventErrorBuilder()
                .WithEvent(ex.JournalEntry.Event)
                .WithAggregateKey(ex.JournalEntry.AggregateKey)
                .WithTrackId(ex.JournalEntry.TrackId)
                .WithException(ex.InnerException)
                .ToEntry();
        }
    }
}