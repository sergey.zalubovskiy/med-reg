﻿namespace Registration.Events.Clinics.ClinicRegistration
{
    public class ClinicRegistered : BaseEvent, IClinicRegistrationEvent
    {
        public string ClinicName { get; set; }

        public string ClinicAddress { get; set; }
    }
}