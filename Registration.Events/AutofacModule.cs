﻿using Autofac;
using Registration.Events.Mappings;

namespace Registration.Events
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<IActionMapper>().As<ActionMapper>().SingleInstance();
        }
    }
}