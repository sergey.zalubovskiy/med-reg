﻿namespace Registration.Actions
{
    public interface ICommand
    {
        string AggregateKey { get; }
        string TrackId { get; }
        IAction Action { get; }
    }
}