﻿namespace Registration.Actions.Clinics.ClinicRegistration
{
    public class ClinicRegistrationCommand : ICommand
    {
        public ClinicRegistrationCommand(string trackId, IClinicRegistrationAction action)
        {
            AggregateKey = "ClinicRegistration";
            TrackId = trackId;
            Action = action;
        }

        #region Implementation of ICommand

        public string AggregateKey { get; }
        public string TrackId { get; }
        public IAction Action { get; }

        #endregion
    }
}