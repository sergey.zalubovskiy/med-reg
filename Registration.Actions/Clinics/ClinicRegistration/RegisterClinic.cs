﻿namespace Registration.Actions.Clinics.ClinicRegistration
{
    public class RegisterClinic : BaseAction, IClinicRegistrationAction
    {
        public string ClinicName { get; set; }

        public string ClinicAddress { get; set; }
    }
}