﻿using System;

namespace Registration.Actions
{
    public abstract class BaseAction : IAction
    {
        protected BaseAction()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
    }
}