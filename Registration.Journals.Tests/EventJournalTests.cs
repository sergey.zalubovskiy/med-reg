﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using FluentAssertions;
using Registration.Journals.Events;
using Registration.Journals.Implementations.Events;
using Xbehave;
using Xunit;

namespace Registration.Journals.Tests
{
    [Collection(nameof(RethinkDbCollection))]
    public class EventJournalTests
    {
        private readonly RethinkDbFixture _fixture;

        private IEventJournal _journal;

        public EventJournalTests(RethinkDbFixture fixture)
        {
            _fixture = fixture;
        }

        [Background]
        public void Background()
        {
            "Create journal".x(
                async () =>
                {
                    var feed = new EventJournal(nameof(EventJournalTests), _fixture.ConnectionProvider);
                    _journal = await feed.EnsureJournalAsync();
                });
        }

        [Scenario]
        public void ListeningItems(
            IEventJournalEntry publishedJournalEntry,
            IObservable<IEventJournalEntry> subscription,
            IEventJournalEntry receivedJournalEntry)
        {
            const string aggregateKey = nameof(ListeningItems);
            const ulong number = 1;

            "Subscribe on journal with aggregate key from beginning".x(
                () => { subscription = _journal.Listen(aggregateKey, 0); });

            "Publish test event".x(
                async () =>
                {
                    publishedJournalEntry = EntryFactory(aggregateKey, number);
                    await _journal.PublishAsync(publishedJournalEntry);
                });

            "Wait for envelope from subscription".x(
                () =>
                {
                    var receiveTask = subscription.SubscribeOn(TaskPoolScheduler.Default).FirstOrDefaultAsync()
                        .ToTask();
                    WaitWithTimeout(receiveTask, TimeSpan.FromSeconds(1));
                    receivedJournalEntry = receiveTask.Result;
                });

            "Received envelope should be equivalent to published".x(
                () =>
                {
                    receivedJournalEntry.Should().BeEquivalentTo(
                        publishedJournalEntry,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void ListeningOldItems(
            IEventJournalEntry publishedJournalEntry,
            IObservable<IEventJournalEntry> subscription,
            IEventJournalEntry receivedJournalEntry)
        {
            const string aggregateKey = nameof(ListeningOldItems);

            "Publish test event".x(
                async () =>
                {
                    publishedJournalEntry = EntryFactory(aggregateKey, 0);
                    await _journal.PublishAsync(publishedJournalEntry);
                });

            "Subscribe on journal with aggregate key from beginning".x(
                () => { subscription = _journal.Listen(aggregateKey, 0); });

            "Wait for envelope from subscription".x(
                () =>
                {
                    var receiveTask = subscription.SubscribeOn(TaskPoolScheduler.Default).FirstOrDefaultAsync()
                        .ToTask();
                    WaitWithTimeout(receiveTask, TimeSpan.FromSeconds(1));
                    receivedJournalEntry = receiveTask.Result;
                });

            "Received envelope should be equivalent to published".x(
                () =>
                {
                    receivedJournalEntry.Should().BeEquivalentTo(
                        publishedJournalEntry,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void ListeningWithFiltration(IObservable<IEventJournalEntry> subscription)
        {
            const string aggregateKey = nameof(ListeningWithFiltration);
            const string wrongAggregateKey = aggregateKey + "_wrong";

            var entries = new List<IEventJournalEntry>
            {
                EntryFactory(aggregateKey, 0),
                EntryFactory(aggregateKey, 1),
                EntryFactory(aggregateKey, 2)
            };
            var wrongEntries = new List<IEventJournalEntry>
            {
                EntryFactory(wrongAggregateKey, 0),
                EntryFactory(wrongAggregateKey, 1),
                EntryFactory(wrongAggregateKey, 2)
            };
            IEnumerable<IEventJournalEntry> receivedEnvelopes = new Collection<IEventJournalEntry>();

            "Subscribe on journal with aggregate key from beginning".x(
                () => { subscription = _journal.Listen(aggregateKey, 0); });

            "Publish events".x(
                async () =>
                {
                    for (var i = 0; i < entries.Count; i++)
                    {
                        await _journal.PublishAsync(entries[i]);
                        await _journal.PublishAsync(wrongEntries[i]);
                    }
                });

            "Wait for envelope from subscription".x(
                () =>
                {
                    var receiveTask = subscription.Take(3).ToList().SubscribeOn(TaskPoolScheduler.Default).ToTask();
                    WaitWithTimeout(receiveTask, TimeSpan.FromSeconds(1));
                    receivedEnvelopes = receiveTask.Result;
                });

            "Received envelopes should be equivalents to published".x(
                () =>
                {
                    receivedEnvelopes.Should().BeEquivalentTo(
                        entries,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void Publishing()
        {
            const string aggregateKey = nameof(Publishing);
            "Publish event".x(
                async () =>
                {
                    var publishedJournalEntry = EntryFactory(aggregateKey, 0);
                    await _journal.PublishAsync(publishedJournalEntry);
                });
        }

        [Scenario]
        public void QueryByAggregate(IEnumerable<IEventJournalEntry> returnedEnvelopes)
        {
            var aggregateKey = nameof(QueryByAggregate);
            var wrongAggregateKey = aggregateKey + "_fail";
            var entries = new List<IEventJournalEntry>
            {
                EntryFactory(aggregateKey, 0),
                EntryFactory(aggregateKey, 1),
                EntryFactory(aggregateKey, 2)
            };
            var wrongEntries = new List<IEventJournalEntry>
            {
                EntryFactory(wrongAggregateKey, 0),
                EntryFactory(wrongAggregateKey, 1),
                EntryFactory(wrongAggregateKey, 2)
            };
            "Publish events".x(
                async () =>
                {
                    for (var i = 0; i < entries.Count; i++)
                    {
                        await _journal.PublishAsync(wrongEntries[i]);
                        await _journal.PublishAsync(entries[i]);
                    }
                });

            "Query event by aggregate key".x(
                async () => { returnedEnvelopes = await _journal.QueryAllAsync(aggregateKey); });

            "Returned envelopes should be equivalents to published".x(
                () =>
                {
                    returnedEnvelopes.Should().BeEquivalentTo(
                        entries,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void QueryByIndex(
            ICollection<IEventJournalEntry> publishedEnvelopes,
            IEnumerable<IEventJournalEntry> returnedEnvelopes)
        {
            var aggregateKey = nameof(QueryByIndex);
            var wrongAggregateKey = aggregateKey + "_fail";
            var entries = new List<IEventJournalEntry>
            {
                EntryFactory(aggregateKey, 0),
                EntryFactory(aggregateKey, 1),
                EntryFactory(aggregateKey, 2),
                EntryFactory(aggregateKey, 3),
                EntryFactory(aggregateKey, 4),
                EntryFactory(aggregateKey, 5)
            };
            var wrongEntries = new List<IEventJournalEntry>
            {
                EntryFactory(wrongAggregateKey, 0),
                EntryFactory(wrongAggregateKey, 1),
                EntryFactory(wrongAggregateKey, 2),
                EntryFactory(wrongAggregateKey, 3),
                EntryFactory(wrongAggregateKey, 4),
                EntryFactory(wrongAggregateKey, 5)
            };

            "Publish events".x(
                async () =>
                {
                    for (var i = 0; i < entries.Count; i++)
                    {
                        await _journal.PublishAsync(wrongEntries[i]);
                        await _journal.PublishAsync(entries[i]);
                    }
                });

            "Query event by index key".x(
                async () => { returnedEnvelopes = await _journal.QueryAsync(aggregateKey, 3); });

            "Returned envelopes should be equivalents to published".x(
                () =>
                {
                    returnedEnvelopes.Should().BeEquivalentTo(
                        new[] {entries[3], entries[4], entries[5]},
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        private static IEventJournalEntry EntryFactory(string aggregateKey, ulong number)
        {
            return new EventJournalEntryBuilder()
                .WithSequenceNumber(number)
                .WithAggregateKey(aggregateKey)
                .WithTrackId(Guid.NewGuid().ToString())
                .WithAction(new TestAction())
                .WithEvent(new TestEvent())
                .ToEntry();
        }

        private static void WaitWithTimeout<T>(Task<T> taskToWait, TimeSpan timeout)
        {
            var executed = taskToWait.Wait(timeout);
            if (executed) return;

            throw new TimeoutException();
        }
    }
}