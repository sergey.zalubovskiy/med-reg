﻿using System;
using Registration.Actions;

namespace Registration.Journals.Tests
{
    public class TestAction : IAction
    {
        public TestAction()
        {
            Id = Guid.NewGuid().ToString();
        }

        #region Implementation of IAction

        public string Id { get; set; }

        #endregion
    }
}