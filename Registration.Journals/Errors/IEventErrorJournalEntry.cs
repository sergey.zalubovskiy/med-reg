﻿using Registration.Events;

namespace Registration.Journals.Errors
{
    /// <summary>
    ///     Contains data for error occured during event processing
    /// </summary>
    public interface IEventErrorJournalEntry : IErrorJournalEntry
    {
        IEvent Event { get; }
    }
}