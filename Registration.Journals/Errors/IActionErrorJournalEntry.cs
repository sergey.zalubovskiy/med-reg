﻿using Registration.Actions;

namespace Registration.Journals.Errors
{
    /// <summary>
    ///     Contains data for error occured during action processing
    /// </summary>
    public interface IActionErrorJournalEntry : IErrorJournalEntry
    {
        IAction Action { get; }
    }
}