﻿using System.Runtime.CompilerServices;
using Autofac;
using Registration.Journals.Errors;
using Registration.Journals.Events;
using Registration.Journals.Implementations.Errors;
using Registration.Journals.Implementations.Events;

[assembly: InternalsVisibleTo("Registration.Journals.Tests")]

namespace Registration.Journals
{
    /// <summary>
    ///     Registers dependencies for Journals implementation based on RethinkDB
    /// </summary>
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventJournalEntryBuilder>().As<IEventJournalEntryBuilder>();
            builder.RegisterType<ActionErrorJournalEntryBuilder>().As<IActionErrorJournalEntryBuilder>();
            builder.RegisterType<EventErrorJournalEntryBuilder>().As<IEventErrorJournalEntryBuilder>();
            builder.RegisterType<ErrorJournal>().As<IErrorJournal>();
            builder.RegisterType<EventJournal>().As<IEventJournal>();
        }
    }
}