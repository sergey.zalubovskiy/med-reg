﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Registration.Journals.Events
{
    /// <summary>
    ///     Creates event journal with specified name
    /// </summary>
    /// <param name="journalName"></param>
    /// <returns></returns>
    public delegate IEventJournal EventJournalFactory(string journalName);

    public interface IEventJournal
    {
        /// <summary>
        ///     Creates event journal in database if not exists
        /// </summary>
        /// <returns></returns>
        Task<IEventJournal> EnsureJournalAsync();

        /// <summary>
        ///     Listens for events posted to journal for specified aggregate since last known index
        /// </summary>
        /// <param name="aggregateKey"></param>
        /// <param name="fromSequenceNumber"></param>
        /// <returns></returns>
        IObservable<IEventJournalEntry> Listen(string aggregateKey, ulong fromSequenceNumber);

        /// <summary>
        ///     Listens for events posted to journal for specified aggregate since last known index
        /// </summary>
        /// <param name="aggregateKey"></param>
        /// <param name="trackId"></param>
        /// <returns></returns>
        IObservable<IEventJournalEntry> Listen(string aggregateKey, string trackId);

        /// <summary>
        ///     Writes event to event stream with specified index and aggregate
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        Task PublishAsync(IEventJournalEntry entry);

        /// <summary>
        ///     Returns all events for specified aggregate
        /// </summary>
        /// <param name="aggregateKey"></param>
        /// <returns></returns>
        Task<IEnumerable<IEventJournalEntry>> QueryAllAsync(string aggregateKey);

        /// <summary>
        ///     Returns events for specified aggregate form with in specified index range including boundaries
        /// </summary>
        /// <param name="aggregateKey"></param>
        /// <param name="fromSequenceNumber"></param>
        /// <returns></returns>
        Task<IEnumerable<IEventJournalEntry>> QueryAsync(string aggregateKey, ulong fromSequenceNumber);
    }
}