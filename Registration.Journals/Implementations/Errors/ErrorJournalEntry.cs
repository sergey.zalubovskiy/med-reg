using System;
using Registration.Journals.Errors;

namespace Registration.Journals.Implementations.Errors
{
    internal abstract class ErrorJournalEntry : BaseJournalEntry, IErrorJournalEntry
    {
        #region Implementation of IErrorJournalEntry

        public Exception Exception { get; set; }

        #endregion
    }

    public interface IErrorJournalEntryBuilder<out TEntry, out TBuilder> : IJournalEntryBuilder<TEntry, TBuilder>
        where TEntry : IErrorJournalEntry
        where TBuilder : IErrorJournalEntryBuilder<TEntry, TBuilder>
    {
        TBuilder WithException(Exception ex);
    }

    internal abstract class ErrorJournalEntryBuilder<TEntry, TEntryImpl, TBuilder> :
        BaseJournalEntryBuilder<TEntry, TEntryImpl, TBuilder>, IErrorJournalEntryBuilder<TEntry, TBuilder>
        where TEntry : IErrorJournalEntry
        where TEntryImpl : ErrorJournalEntry, TEntry
        where TBuilder : IErrorJournalEntryBuilder<TEntry, TBuilder>
    {
        #region Implementation of IErrorJournalEntryBuilder<out TEntry,out TBuilder>

        public TBuilder WithException(Exception ex)
        {
            Entry.Exception = ex;
            return This;
        }

        #endregion
    }
}