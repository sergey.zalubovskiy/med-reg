﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Registration.Journals.Errors;
using Registration.RethinkDb.Common;

namespace Registration.Journals.Implementations.Errors
{
    /// <summary>
    ///     The error journal.
    /// </summary>
    internal class ErrorJournal : BaseJournal, IErrorJournal
    {
        public ErrorJournal(string journalName, IRethinkConnectionProvider connectionProvider) : base(journalName,
            "errors", connectionProvider)
        {
        }

        private async Task PublishAsync<TEnvelope>(TEnvelope envelope)
            where TEnvelope : IErrorJournalEntry
        {
            await Table.Insert(envelope)
                .OptArg("durability", "hard")
                .OptArg("conflict", "error")
                .RunAsync(Connection);
        }

        #region Implementation of IErrorJournal

        public async Task<IErrorJournal> EnsureJournalAsync()
        {
            await EnsureJournalTableAsync();
            await Connection.EnsureIndexAsync(
                DbName,
                JournalTableName,
                nameof(IErrorJournalEntry.TrackId));
            return this;
        }

        public IObservable<IErrorJournalEntry> Listen(string trackId)
        {
            return Table.GetAll(trackId)
                .OptArg("index", nameof(IErrorJournalEntry.TrackId))
                .Changes()
                .OptArg("include_initial", true)
                .RunChanges<IErrorJournalEntry>(Connection).ToObservable()
                .Select(c => c.NewValue);
        }

        public Task PublishActionAsync(IActionErrorJournalEntry entry)
        {
            return PublishAsync(entry);
        }

        public Task PublishEventAsync(IEventErrorJournalEntry entry)
        {
            return PublishAsync(entry);
        }

        #endregion
    }
}