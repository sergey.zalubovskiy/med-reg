﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using Registration.RethinkDb.Common;
using RethinkDb.Driver;
using RethinkDb.Driver.Ast;
using RethinkDb.Driver.Net;

namespace Registration.Journals.Implementations
{
    internal abstract class BaseJournal
    {
        protected static readonly RethinkDB R = RethinkDB.R;
        private readonly IRethinkConnectionProvider _connectionProvider;

        private readonly string _journalName;

        private readonly string _journalPrefix;

        protected BaseJournal(string journalName, string journalPrefix, IRethinkConnectionProvider connectionProvider)
        {
            _journalName = journalName;
            _journalPrefix = journalPrefix;
            _connectionProvider = connectionProvider;
        }

        protected Connection Connection => _connectionProvider.Connection;

        protected string DbName => Connection.Db;

        protected string JournalTableName => $"journal_{_journalPrefix}_{_journalName}";

        protected Table Table => Db.Table(JournalTableName);

        private Db Db => R.Db(DbName);

        protected async Task<BaseJournal> EnsureJournalTableAsync()
        {
            Converter.Serializer.TypeNameHandling = TypeNameHandling.All;
            await Connection.EnsureDatabaseAsync(DbName);
            await Connection.EnsureTableAsync(DbName, JournalTableName);
            return this;
        }
    }
}