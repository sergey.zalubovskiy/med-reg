﻿using Registration.Actions;
using Registration.Events;
using Registration.Journals.Events;

namespace Registration.Journals.Implementations.Events
{
    internal sealed class EventJournalEntry : BaseJournalEntry, IEventJournalEntry
    {
        #region Implementation of IEventJournalEntry

        public IAction Action { get; set; }
        public ulong SequenceNumber { get; set; }
        public IEvent Event { get; set; }

        #endregion
    }

    public interface IEventJournalEntryBuilder : IJournalEntryBuilder<IEventJournalEntry, IEventJournalEntryBuilder>
    {
        IEventJournalEntryBuilder WithAction(IAction action);
        IEventJournalEntryBuilder WithSequenceNumber(ulong sequenceNumber);
        IEventJournalEntryBuilder WithEvent(IEvent @event);
    }

    internal class EventJournalEntryBuilder :
        BaseJournalEntryBuilder<IEventJournalEntry, EventJournalEntry, IEventJournalEntryBuilder>,
        IEventJournalEntryBuilder
    {
        #region Implementation of IEventJournalEntryBuilder<EventJournalEntry,out EventJournalEntryBuilder>

        public IEventJournalEntryBuilder WithAction(IAction action)
        {
            Entry.Action = action;
            return this;
        }

        public IEventJournalEntryBuilder WithSequenceNumber(ulong sequenceNumber)
        {
            Entry.SequenceNumber = sequenceNumber;
            return this;
        }

        public IEventJournalEntryBuilder WithEvent(IEvent @event)
        {
            Entry.Event = @event;
            return this;
        }

        #endregion

        #region Overrides of BaseJournalEntryBuilder<IEventJournalEntry, EventJournalEntry, IEventJournalEntryBuilder>

        protected override EventJournalEntry CreateEntry()
        {
            return new EventJournalEntry();
        }

        protected override string GetId()
        {
            return $"{Entry.AggregateKey}_{Entry.SequenceNumber}";
        }

        protected override IEventJournalEntryBuilder This => this;

        #endregion
    }
}